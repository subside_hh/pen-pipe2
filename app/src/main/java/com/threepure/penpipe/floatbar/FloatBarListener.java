package com.threepure.penpipe.floatbar;

/**
 * @author ThreePure
 * @date 2021/11/15 8:39
 * @description: 悬浮菜单展开后各个按钮的点击事件监听接口
 * @since 1.8
 */
public interface FloatBarListener {
    /**
     * unDo的监听方法
     */
    void unDoClick();

    /**
     * reDo的监听方法
     */
    void reDoClick();

    /**
     * pen的监听方法
     */
    void penClick();

    /**
     * 橡皮擦的监听方法
     */
    void eraserClick();

    /**
     * 保存的监听方法
     */
    void savaClick();

    /**
     * 设置的监听方法
     */
    void setClick();
}
