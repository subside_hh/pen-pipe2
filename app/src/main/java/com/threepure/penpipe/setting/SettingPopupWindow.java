package com.threepure.penpipe.setting;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.annotation.RequiresApi;

import com.github.iielse.switchbutton.SwitchView;
import com.threepure.penpipe.MainActivity;
import com.threepure.penpipe.R;
import com.threepure.penpipe.util.Config;
import com.threepure.penpipe.util.colorpicker.ColorPickerDialog;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.IndicatorType;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;
import com.warkiz.widget.TickMarkType;

/**
 * @author ThreePure
 * @date 2021/11/23 16:08
 * @description: 设置按钮的弹窗
 *  定时删除
 *  橡皮宽度
 *  导入图片
 *  背景颜色
 *  分享
 *  画笔颜色
 *  文字水印
 * @since 1.8
 */
public class SettingPopupWindow  extends PopupWindow {
    private static final String TAG = "TP_SettingPopupWindow";

    private Context context;
    private View rootView;

    private LinearLayout timer_container;
    /**开启定时清屏开关*/
    private SwitchView switchButton;
    /**自定义水印文字开关*/
    private SwitchView switchButton2;
    //private LinearLayout eraser_size_container;
    //private IndicatorSeekBar seekbar;
    private LinearLayout import_pic_container;
    private LinearLayout bg_color_ontainer;
    private LinearLayout share_ontainer;

    private SettingPopupWindow.OnClickSettingListener settingListener;

    public SettingPopupWindow(Context context) {
        super(context);
        this.context = context;

        rootView = LayoutInflater.from(context).inflate(R.layout.setting_pop, null);

        timer_container = rootView.findViewById(R.id.timer_container);
        switchButton = (SwitchView)rootView.findViewById(R.id.sw_btn);
        switchButton2 = (SwitchView)rootView.findViewById(R.id.sw_btn_textMask);
        //eraser_size_container = rootView.findViewById(R.id.eraser_size_container2);
        //seekbar = (IndicatorSeekBar)rootView.findViewById(R.id.seekBar);
        import_pic_container = rootView.findViewById(R.id.import_pic_container);
        bg_color_ontainer = rootView.findViewById(R.id.bg_color_container);
        share_ontainer = rootView.findViewById(R.id.share_ontainer);

        onClickListener();
        this.setContentView(rootView);
        this.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.update();
    }

    private void onClickListener() {
        timer_container.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                Log.e(TAG, "onClick: 从新设置定时清屏时间" );
                IndicatorSeekBar seekbar = IndicatorSeekBar
                        .with(context)
                        .max(60)
                        .min(2)
                        .progress(Config.TIME_OF_CLEAR_SCREEN / 1000)
                        .showTickMarksType(TickMarkType.OVAL)
                        .tickMarksColor(context.getResources().getColor(R.color.color_blue, null))
                        .tickMarksSize(13)
                        .showTickTexts(true)
                        .tickTextsColor(context.getResources().getColor(R.color.color_pink))
                        .tickTextsSize(13)
                        .tickTextsTypeFace(Typeface.MONOSPACE)
                        .showIndicatorType(IndicatorType.ROUNDED_RECTANGLE)
                        .indicatorColor(context.getResources().getColor(R.color.color_blue))
                        .indicatorTextColor(Color.parseColor("#ffffff"))
                        .indicatorTextSize(13)
                        .thumbColor(context.getResources().getColor(R.color.colorAccent, null))
                        .thumbSize(14)
                        .trackProgressColor(context.getResources().getColor(R.color.colorAccent,null))
                        .trackProgressSize(4)
                        .trackBackgroundColor(context.getResources().getColor(R.color.color_gray))
                        .trackBackgroundSize(2)
                        .build();
                //添加时间单位
                seekbar.setIndicatorTextFormat("${PROGRESS} s");

                seekbar.setOnSeekChangeListener(new OnSeekChangeListener() {
                    @Override
                    public void onSeeking(SeekParams seekParams) {
                        Log.e(TAG, "onSeeking: seekParams.progress:  " + seekParams.progress );
                        Config.TIME_OF_CLEAR_SCREEN = seekParams.progress * 1000;
                        if (Config.openTimer && settingListener != null){
                            settingListener.onTimerSetting(true);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(IndicatorSeekBar seekBar) {
                    }

                    @Override
                    public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
                    }
                });

                AlertDialog.Builder isExit = new AlertDialog.Builder(context);
                isExit.setTitle("清屏时长");
                isExit.setView(seekbar);
                isExit.setCancelable(true);
                isExit.show();
            }
        });


        //开关按钮事件监听
        switchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isOpened = switchButton.isOpened();
                Config.openTimer = isOpened;
                Log.e(TAG, "onClick: isOpened:" + isOpened);
                if (settingListener != null){
                    settingListener.onTimerSetting(isOpened);
                }
            }
        });

        //监听是否自定义文字水印
        switchButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isOpened = switchButton2.isOpened();
                if (settingListener != null){
                    settingListener.onTextMaskSetting(isOpened);
                }
            }
        });

        //seekBar状态监听
        /*seekbar.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                //具体的整数值。
                if (settingListener != null) {
                    settingListener.onEraserWidthSetting(seekParams.progress);
                }
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
            }
        });*/

        //导入图片事件监听
        import_pic_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settingListener != null){
                    settingListener.onImportPicSetting();
                }
            }
        });

        //更改背景颜色事件监听
        bg_color_ontainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorPickerDialog dialog = new ColorPickerDialog(getContentView().getContext(),Color.BLACK);
                dialog.setOnColorChangedListener(new MainActivity.BackgroundColorListener());
                dialog.show();
            }
        });

        //分享图片事件监听
        share_ontainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settingListener != null) {
                    //传递给MainActivity中具体去实现
                    settingListener.onShareSetting(true);
                }
            }
        });

    }

    /**
     * @description: 显示在右下角
     * @Return: void
     **/
    public void popAtBottomRight() {

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(0, 0, 0, 0);
        lp.gravity = Gravity.CENTER;
        timer_container.setLayoutParams(lp);
        //eraser_size_container.setLayoutParams(lp);
        import_pic_container.setLayoutParams(lp);
        bg_color_ontainer.setLayoutParams(lp);
        share_ontainer.setLayoutParams(lp);
        rootView.setBackgroundResource(R.drawable.popup_windows_shape);
    }

    /**
     * @description: 设置seekBar的宽度
     * @param progress  当前橡皮宽度设置给seekBar
     * @Return: void
     **/
    /*public void setSeekBarProgress(int progress){
        seekbar.setProgress(progress);
    }*/

    /**
     * @description:   设置定时器SwitchButton的状态
     * @param isOpen   定时器是否打开
     * @Return: void
     **/
    public void setSwitchButtonAction(boolean isOpen){
        switchButton.setOpened(isOpen);
    }

    /**
     * @description:   设置定时器SwitchButton2的状态
     * @param isOpen   定时器是否打开
     * @Return: void
     **/
    public void setSwitchButton2Action(boolean isOpen){
        switchButton2.setOpened(isOpen);
    }


    public void setSettingListener(OnClickSettingListener settingListener) {
        this.settingListener = settingListener;
    }


    public interface OnClickSettingListener {
        /**是否开启pencil模式*/
        //void onPencilModeSetting();
        /**定时器监听*/
        void onTimerSetting(boolean isOpen);

        /**橡皮宽度模式*/
        //void onEraserWidthSetting(int width);

        /**导入图片作为背景*/
        void onImportPicSetting();

        /**更改背景颜色监听*/
        void onBgColorSetting();

        /**分享图片*/
        void onShareSetting(boolean isShare);

        /**文字水印效果*/
        void onTextMaskSetting(boolean isOpen);
    }

}
