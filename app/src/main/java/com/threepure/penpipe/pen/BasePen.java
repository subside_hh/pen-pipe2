package com.threepure.penpipe.pen;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

import com.threepure.penpipe.util.Bezier;
import com.threepure.penpipe.util.ControllerPoint;
import com.threepure.penpipe.util.MotionElement;

import java.util.ArrayList;

/**
 * @author ThreePure
 * @date 2021/11/17 9:02
 * @description: 画笔基类(抽象类——由子类实现)
 * @since 1.8
 */
public abstract class BasePen {

    /**画笔绘制计算的次数，数值越小计算的次数越多*/
    public static final int STEP_FACTOR = 20;

    /**点的集合*/
    protected ArrayList<ControllerPoint> mHWPointList = new ArrayList<>();
    /**控制点*/
    protected ControllerPoint mLastPoint = new ControllerPoint(0, 0);
    protected ControllerPoint mCurPoint;

    /**创建一个贝塞尔曲线实例*/
    protected Bezier mBezier = new Bezier();

    /**画笔*/
    protected Paint mPaint;

    /**
     * 笔的宽度信息
     */
    private double mBaseWidth;

    private double mLastVel;
    private double mLastWidth;

    /**记录最先/最后的手指id*/
    private int lastId = 0;


    public void draw(Canvas canvas) {
        mPaint.setStyle(Paint.Style.FILL);
        //点的集合少 不去绘制
        if (mHWPointList == null || mHWPointList.size() < 1) {
            return;
        }
        mCurPoint = mHWPointList.get(0);
        doPreDraw(canvas);
    }

    /**
     * @description:  绘制 (当现在的点和触摸点的位置在一起的时候不用去绘制)
     * @Param: [android.graphics.Canvas, com.threepure.penpipe.util.ControllerPoint, android.graphics.Paint]
     * @Return: void
     */
    protected void drawToPoint(Canvas canvas, ControllerPoint point, Paint paint) {
        if ((mCurPoint.x == point.x) && (mCurPoint.y == point.y)) {
            return;
        }
        doDraw(canvas, point, paint);
    }

    /**
     * @description: 触摸事件
     * @Param: [android.view.MotionEvent, android.graphics.Canvas]
     * @Return: boolean
     */
    public boolean onTouchEvent(MotionEvent event, Canvas canvas) {
        // event会被下一次事件重用，这里必须生成新的，否则会有问题
        int action = event.getAction() & event.getActionMasked();
        MotionEvent event2 = MotionEvent.obtain(event);

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                lastId = event2.getPointerId(0);
                onDown(createMotionElement(event2), canvas);
                return true;
            case MotionEvent.ACTION_POINTER_DOWN:
                lastId = 0;
                mLastVel = 0;
                mLastPoint = new ControllerPoint(event2.getX(event2.getActionIndex()), event2.getY(event2.getActionIndex()));
                break;
            case MotionEvent.ACTION_MOVE:
                if (lastId != event2.getPointerId(event2.getActionIndex())) {
                    return true;
                }
                onMove(createMotionElement(event2), canvas);
                return true;
            case MotionEvent.ACTION_POINTER_UP:
                onUp(createMotionElement(event2), canvas);
                return true;
            case MotionEvent.ACTION_UP:
                lastId = event2.getPointerId(0);
                onUp(createMotionElement(event2), canvas);
                return true;
            default:
                break;
        }
        return false;
    }

    /**
     * @description:  按下时的事件
     * @Param: [com.threepure.penpipe.util.MotionElement, android.graphics.Canvas]
     * @Return: void
     */
    public void onDown(MotionElement mElement, Canvas canvas) {
        if (mPaint == null) {
            throw new NullPointerException("paint不能为null");
        }
        if (getNewPaint(mPaint) != null) {
            Paint paint = getNewPaint(mPaint);
            mPaint = paint;
            paint = null;
        }
        mHWPointList.clear();
        //记录down的控制点的信息
        ControllerPoint curPoint = new ControllerPoint(mElement.x, mElement.y);
        mLastWidth = 0.7 * mBaseWidth;
        //down下的点的宽度
        curPoint.width = (float) mLastWidth;
        mLastVel = 0;
        //记录当前的点
        mLastPoint = curPoint;
    }

    /**
     * @description:  手指移动的事件
     * @Param: [com.threepure.penpipe.util.MotionElement, android.graphics.Canvas]
     * @Return: void
     */
    public void onMove(MotionElement mElement, Canvas canvas) {
        ControllerPoint curPoint = new ControllerPoint(mElement.x, mElement.y);
        double deltaX = curPoint.x - mLastPoint.x;
        double deltaY = curPoint.y - mLastPoint.y;
        //当滑动的越快的话，deltaX+deltaY的值越大，这个越大的话，curDis也越大
        double curDis = Math.hypot(deltaX, deltaY);
        //这个值越小，画的点或者是绘制椭圆形越多，这个值越大的话，绘制的越少，笔就越细，宽度越小
        double curVel = curDis * PenConfig.DIS_VEL_CAL_FACTOR;
        double curWidth;
        //点的集合少，我们得必须改变宽度,每次点击的down的时候，这个事件
        if (mHWPointList.size() < 2) {

            curWidth = calcNewWidth(curVel, mLastVel, curDis, 1.7,
                    mLastWidth);
            curPoint.width = (float) curWidth;
            mBezier.init(mLastPoint, curPoint);
        } else {
            mLastVel = curVel;
            curWidth = calcNewWidth(curVel, mLastVel, curDis, 1.7,mLastWidth);
            curPoint.width = (float) curWidth;
            mBezier.addNode(curPoint);
        }
        //每次移动的话，这里赋值新的值
        mLastWidth = curWidth;
        doMove(curDis);
        mLastPoint = curPoint;
    }

    /**
     * @description: 手指抬起来的事件
     * @Param: [com.threepure.penpipe.util.MotionElement, android.graphics.Canvas]
     * @Return: void
     */
    public void onUp(MotionElement mElement, Canvas canvas) {
        if (mHWPointList.size() == 0) {
            return;
        }
        mCurPoint = new ControllerPoint(mElement.x, mElement.y);
        double deltaX = mCurPoint.x - mLastPoint.x;
        double deltaY = mCurPoint.y - mLastPoint.y;
        // 返回无中间溢出或下溢的sqrt（x2+y2）。
        double curDis = Math.hypot(deltaX, deltaY);
        mCurPoint.width = 0;

        mBezier.addNode(mCurPoint);

        int steps = 1 + (int) curDis / STEP_FACTOR;
        double step = 1.0 / steps;
        for (double t = 0; t < 1.0; t += step) {
            ControllerPoint point = mBezier.getPoint(t);
            mHWPointList.add(point);
        }
        mBezier.end();
        for (double t = 0; t < 1.0; t += step) {
            ControllerPoint point = mBezier.getPoint(t);
            mHWPointList.add(point);
        }
        draw(canvas);
        clear();
    }

    /**
     * @description:  计算新的宽度信息
     * @Param: [double, double, double, double, double]
     * @Return: double
     */
    public double calcNewWidth(double curVel, double lastVel, double curDis,double factor, double lastWidth) {
        double calVel = curVel * 0.6 + lastVel * (1 - 0.6);
        double vfac = Math.log(factor * 2.0f) * (-calVel);
        double calWidth = mBaseWidth * Math.exp(vfac);
        return calWidth;
    }

    /**
     * @description:  创建触摸点信息
     * @Param: [android.view.MotionEvent]
     * @Return: com.threepure.penpipe.util.MotionElement
     */
    public MotionElement createMotionElement(MotionEvent motionEvent) {
        MotionElement motionElement = new MotionElement(motionEvent.getX(0), motionEvent.getY(0),motionEvent.getPressure(), motionEvent.getToolType(0));
        return motionElement;
    }

    /**
     * @description: 清除缓存的控制点
     * @Param: []
     * @Return: void
     */
    public void clear() {
        mHWPointList.clear();
    }

    /**
     * @description:  获取画笔
     * @Param: []
     * @Return: android.graphics.Paint
     */
    public Paint getmPaint() {
        return mPaint;
    }

    /**
     * @description: 设置Paint（画笔）的方法
     * @Param: [android.graphics.Paint]
     * @Return: void
     */
    public void setPaint(Paint mPaint) {
        this.mPaint = mPaint;
        //获取画笔的宽度
        mBaseWidth = mPaint.getStrokeWidth();
    }

    /**
     * @description:  判断画笔是否为空
     * @Param: []
     * @Return: boolean
     */
    public boolean isNullPaint() {
        return mPaint == null;
    }

    protected Paint getNewPaint(Paint paint) {
        return null;
    }

    /**
     * @description:  移动的时候的处理方法
     * @Param: [double]
     * @Return: void
     */
    protected abstract void doMove(double f);

    /**
     * @description:  绘制方法
     * @Param: [android.graphics.Canvas, com.threepure.penpipe.util.ControllerPoint, android.graphics.Paint]
     * @Return: void
     */
    protected abstract void doDraw(Canvas canvas, ControllerPoint point, Paint paint);

    /**
     * @description: onDraw之前的操作
     * @Param: [android.graphics.Canvas]
     * @Return: void
     */
    protected abstract void doPreDraw(Canvas canvas);

}
