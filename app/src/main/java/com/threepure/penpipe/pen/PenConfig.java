package com.threepure.penpipe.pen;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;

/**
 * @author 画笔配置
 * @date 2021/11/16 14:57
 * @description: 画笔的配置
 * @since 1.8
 */
public class PenConfig {

    /**
     * 画笔大小等级下标
     */
    public static int PAINT_SIZE_LEVEL = 2;

    /**
     * 画笔颜色下标
     */
    public static int PAINT_COLOR_LEVEL = 2;

    /**
     * 笔锋控制值,越小笔锋越粗
     */
    public static final float DIS_VEL_CAL_FACTOR = 0.008f;


    /**
     * 主题颜色
     */
    public static int THEME_COLOR = Color.parseColor("#0c53ab");

    public static  final String SAVE_PATH  = "path";
    private static final String SP_SETTING = "sp_pen_setting";

    /**
     * jpg格式
     */
    public static final String FORMAT_JPG = "JPG";
    /**
     * png格式
     */
    public static final String FORMAT_PNG = "PNG";

    /**
     * @description:  保存画笔颜色设置
     * @Param: [android.content.Context, int]
     * @Return: void
     */
    public static void savePaintColor(Context context, int index) {
        SharedPreferences sp = context.getSharedPreferences(SP_SETTING, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("colorIndex", index);
        editor.apply();
    }

    /**
     * @description:  获取画笔颜色
     * @Param: [android.content.Context]
     * @Return: int
     */
    public static int getPaintColor(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SP_SETTING, Context.MODE_PRIVATE);
        return sp.getInt("colorIndex", PAINT_COLOR_LEVEL);
    }

    /**
     * @description:  保存画笔大小level
     * @Param: [android.content.Context, int]
     * @Return: void
     */
    public static void savePaintTextLevel(Context context, int index) {
        SharedPreferences sp = context.getSharedPreferences(SP_SETTING, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("sizeIndex", index);
        editor.apply();
    }

    /**
     * @description:  从sp文件中获取选中画笔大小level
     * @Param: [android.content.Context]
     * @Return: int
     */
    public static int getPaintTextLevel(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SP_SETTING, Context.MODE_PRIVATE);
        return sp.getInt("sizeIndex", PAINT_SIZE_LEVEL);
    }

    /**
     * @description:  获取是否第一次打开
     * @Param: [android.content.Context]
     * @Return: boolean
     */
    public static boolean getFirst(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SP_SETTING, Context.MODE_PRIVATE);
        return sp.getBoolean("isFirst", true);
    }


    /**
     * @description:  设置是否第一次打开
     * @Param: [android.content.Context, boolean]
     * @Return: void
     */
    public static void setFirst(Context context, boolean isFirst) {
        SharedPreferences sp = context.getSharedPreferences(SP_SETTING, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("isFirst", isFirst);
        editor.apply();
    }
}

