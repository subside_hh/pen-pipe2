package com.threepure.penpipe.pen.setting;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.threepure.penpipe.R;
import com.threepure.penpipe.pen.PenConfig;
import com.threepure.penpipe.util.Config;

/**
 * @author ThreePure
 * @date 2021/11/15 20:55
 * @description: 点击画笔弹出的PopupWindow
 * @since 1.8
 */
public class PenSettingPopupWindow extends PopupWindow {

    private static final String TAG = "TP_PenSettingPopupWindow:";

    private Context context;
    private View rootView;

    private ColorView lastSelectColorView;
    private CircleView lastSelectSizeView;
    private int selectColor;

    private OnSettingListener settingListener;

    public PenSettingPopupWindow(Context context) {
        super(context);
        this.context = context;
        init();
    }

    private void init() {
        rootView = LayoutInflater.from(context).inflate(R.layout.pen_setting_pop, null);

        LinearLayout sizeContainer = rootView.findViewById(R.id.size_container);
        for (int i = 0; i < sizeContainer.getChildCount(); i++) {
            final int index = i;
            final CircleView circleView = (CircleView) sizeContainer.getChildAt(i);

            // 如果这个圆的大小与笔的大小相同，那么这个圆就要设置外边框。并将这个圆存储在lastSelectSizeView中。
            if (circleView.getRadiusLevel() == PenConfig.PAINT_SIZE_LEVEL) {
                circleView.setShowBorder(true);
                lastSelectSizeView = circleView;
            }
            // 为圆设置监听。
            circleView.setOnClickListener(v -> {
                // 如果存储的lastSelectSizeView不为空，接就是每次点击选择过了笔画粗细，那么消除之前被选择的控件显示的外边框
                if (lastSelectSizeView != null) {
                    lastSelectSizeView.setShowBorder(false);
                }
                //显示当前被选中的控件的外边框
                circleView.setShowBorder(true);
                //将这一次选中的控件存储起来
                lastSelectSizeView = circleView;

                //画笔大小等级赋值给PenConfig类的PAINT_SIZE_LEVEL属性
                PenConfig.PAINT_SIZE_LEVEL = index;
                //保存画笔大小level到sp文件，
                PenConfig.savePaintTextLevel(context, index);
                if (settingListener != null) {
                    //设置监听，从子类重写完成
                    settingListener.onSizeSetting(index);
                }
            });
        }

        LinearLayout colorContainer = rootView.findViewById(R.id.color_container);
        for (int i = 0; i < colorContainer.getChildCount(); i++) {
            final int index = i;
            final ColorView colorView = (ColorView) colorContainer.getChildAt(i);

            // 如果这个颜色环与笔的颜色相同，那么这个圆环就要打勾。并将这个颜色环存储在lastSelectColorView中。
            if (colorView.getColorValue() == Config.PEN_COLORS[PenConfig.PAINT_COLOR_LEVEL]) {
                colorView.setSelect(true);
                lastSelectColorView = colorView;
            }
            //为颜色环设置监听
            colorView.setOnClickListener(v -> {
                if (lastSelectColorView != null) {
                    lastSelectColorView.setSelect(false);
                }
                //当前被选中的颜色控件中间打勾
                colorView.setSelect(true);
                //将这一次选中的控件存储起来
                lastSelectColorView = colorView;

                //selectColor就是一个具体的颜色16进制
                selectColor = Config.PEN_COLORS[index];

                PenConfig.PAINT_COLOR_LEVEL = index;
                //保存画笔颜色下标到sp文件，
                PenConfig.savePaintColor(context, index);
                if (settingListener != null) {
                    //监听对象返回的是颜色的下标，可以在重写后根据下边获取具体的颜色。
                    settingListener.onColorSetting(index);
                }
            });
        }

        /*this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);*/
        this.setContentView(rootView);
        this.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.update();
    }

    /**
     * 显示在左上角
     */
    public void popAtTopLeft() {
        View sv = rootView.findViewById(R.id.size_container);
        View cv = rootView.findViewById(R.id.color_container);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //以像素为单位设置边距
        lp.setMargins(0, 0, 0, 0);
        lp.gravity = Gravity.CENTER;
        sv.setLayoutParams(lp);
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp1.setMargins(0, 0, 0, 0);
        lp1.gravity = Gravity.CENTER;
        cv.setLayoutParams(lp1);
        rootView.setBackgroundResource(R.drawable.popup_windows_shape);
    }

    /**
     * 显示在右上角
     */
    public void popAtTopRight() {
        View sv = rootView.findViewById(R.id.size_container);
        View cv = rootView.findViewById(R.id.color_container);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 0, 0, 0);
        lp.gravity = Gravity.CENTER;
        sv.setLayoutParams(lp);
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp1.setMargins(0, 0, 0, 0);
        lp1.gravity = Gravity.CENTER;
        cv.setLayoutParams(lp1);
        rootView.setBackgroundResource(R.drawable.popup_windows_shape);
    }

    /**
     * 显示在右下角
     */
    public void popAtBottomRight() {
        View sv = rootView.findViewById(R.id.size_container);
        View cv = rootView.findViewById(R.id.color_container);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 0, 0, 0);
        lp.gravity = Gravity.CENTER;
        sv.setLayoutParams(lp);
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp1.setMargins(0, 0, 0, 0);
        lp1.gravity = Gravity.CENTER;
        cv.setLayoutParams(lp1);
        rootView.setBackgroundResource(R.drawable.sign_bottom_right_pop_bg);
    }

    /**
     * 显示在左边
     */
    public void popAtLeft() {
        View sv = rootView.findViewById(R.id.size_container);
        View cv = rootView.findViewById(R.id.color_container);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 0, 0, 0);
        lp.gravity = Gravity.CENTER;
        sv.setLayoutParams(lp);
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp1.setMargins(0, 0, 0, 0);
        lp1.gravity = Gravity.CENTER;
        cv.setLayoutParams(lp1);
        rootView.setBackgroundResource(R.drawable.sign_left_pop_bg);
    }

    public interface OnSettingListener {
        void onSizeSetting(int index);
        void onColorSetting(int index);
    }

    public void setSettingListener(OnSettingListener settingListener) {
        this.settingListener = settingListener;
    }
}


