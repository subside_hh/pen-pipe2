package com.threepure.penpipe.pen.setting;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.threepure.penpipe.R;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

/**
 * @author ThreePure
 * @date 2021/11/29 10:55
 * @description: 点击橡皮弹出的PopupWindow
 * @since 1.8
 */
public class EraserSettingPopupWindow extends PopupWindow {

    private static final String TAG = "TP_EraserSettingPopupWindow:";

    private Context context;
    private View rootView;

    private LinearLayout eraser_size_container;
    private IndicatorSeekBar seekbar;

    private OnEraserSettingListener eraserSettingListener;

    public EraserSettingPopupWindow(Context context) {
        super(context);
        this.context = context;
        init();
    }

    private void init() {
        rootView = LayoutInflater.from(context).inflate(R.layout.eraser_setting_pop, null);

        eraser_size_container = rootView.findViewById(R.id.eraser_size_container);
        seekbar = (IndicatorSeekBar)rootView.findViewById(R.id.seekBar);
        //seekBar状态监听
        seekbar.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                //具体的整数值。
                if (eraserSettingListener != null) {
                    eraserSettingListener.onEraserWidthSetting(seekParams.progress);
                }
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
            }
        });

        this.setContentView(rootView);
        this.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.update();
    }


    /**
     * 显示在左上角
     */
    public void popAtTopLeft() {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(0, 0, 0, 0);
        lp.gravity = Gravity.CENTER;
        eraser_size_container.setLayoutParams(lp);
        rootView.setBackgroundResource(R.drawable.popup_windows_shape);
    }

    /**
     * 显示在右上角
     */
    public void popAtTopRight() {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(0, 0, 0, 0);
        lp.gravity = Gravity.CENTER;
        eraser_size_container.setLayoutParams(lp);
        rootView.setBackgroundResource(R.drawable.popup_windows_shape);
    }

    /**
     * 显示在右下角
     */
    public void popAtBottomRight() {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(0, 0, 0, 0);
        lp.gravity = Gravity.CENTER;
        eraser_size_container.setLayoutParams(lp);
        rootView.setBackgroundResource(R.drawable.popup_windows_shape);
    }

    /**
     * 显示在左边
     */
    public void popAtLeft() {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(0, 0, 0, 0);
        lp.gravity = Gravity.CENTER;
        eraser_size_container.setLayoutParams(lp);
        rootView.setBackgroundResource(R.drawable.popup_windows_shape);
    }

    /**
     * @description: 设置seekBar的宽度
     * @param progress  当前橡皮宽度设置给seekBar
     * @Return: void
     **/
    public void setSeekBarProgress(int progress){
        seekbar.setProgress(progress);
    }

    public void setEraserSettingListener(OnEraserSettingListener eraserSettingListener) {
        this.eraserSettingListener = eraserSettingListener;
    }

    /**
     * @description: 对橡皮大小进行设置监听的接口
     **/
    public interface OnEraserSettingListener {
        /**橡皮宽度模式*/
        void onEraserWidthSetting(int width);
    }

}


