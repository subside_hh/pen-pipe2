package com.threepure.penpipe.pen.setting;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.threepure.penpipe.R;
import com.threepure.penpipe.util.Config;

/**
 * @author ThreePure
 * @date 2021/11/15 20:52
 * @description: 画笔颜色的圆圈
 * @since 1.8
 */
public class ColorView extends View {

    private Context context;
    private Paint paint;
    private Path path;

    /**圆的颜色*/
    private int colorValue;
    /**是否被选择*/
    private boolean isSelect;
    /**颜色圈的宽度[圆半径、直径]*/
    private int mStrokeWidth = 12;

    public ColorView(Context context) {
        this(context, null);
    }

    public ColorView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ColorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray ta  = context.obtainStyledAttributes(attrs, R.styleable.ColorView);
        // 获取布局文件中的下标，
        int anInt = ta.getInteger(R.styleable.ColorView_penColorIndex, 0);
        //通过下标，获取Config中对应下标的颜色
        colorValue     = Config.PEN_COLORS[anInt];
        //回收Darray类型，供以后的调用方重新使用。调用此函数后，不得再次触摸键入的数组
        ta.recycle();
        //初始化画笔等
        init();
    }

    /**
     * @description:  画笔初始化
     * @Param: []
     * @Return: void
     */
    private void init() {
        paint = new Paint();
        path = new Path();
        //设置画笔的颜色
        paint.setColor(colorValue);
        //设置画笔的粗细
        paint.setStrokeWidth(mStrokeWidth);
        //填充的方式为描边
        paint.setStyle(Paint.Style.STROKE);
        //画笔设置防止抖动
        paint.setDither(true);
        //画笔设置“防锯齿”
        paint.setAntiAlias(true);
        //设置油漆的接缝
        paint.setStrokeJoin(Paint.Join.ROUND);
    }

    /**
     * @description:  重写父类View中的方法，用于绘制具体的图形
     * @Param: [android.graphics.Canvas]
     * @Return: void
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float cx = getWidth()  / 2.0f;
        float cy = getHeight() / 2.0f;
        float radius = Math.min(getWidth(), getHeight());
        //要绘制的圆的半径
        radius = ( radius - 60 ) / 2.0f;
        //绘制颜色环
        canvas.drawCircle(cx, cy, radius, paint);

        path.moveTo(cx - 20, cy);
        path.lineTo(cx, cy + 20 );
        path.lineTo(cx + 20, cy - 20);

        if( isSelect ){
            Paint.Style paintStyle = paint.getStyle();
            paint.setColor(Color.WHITE);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawPath(path, paint);
            paint.setColor(colorValue);
            paint.setStyle(paintStyle);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width  = onMeasureR(0, widthMeasureSpec);
        int height = onMeasureR(1, heightMeasureSpec);
        setMeasuredDimension(width, height);
    }

    /**
     * @description: 用于计算控件的尺寸
     * @Param: [int, int]
     * @Return: int
     */
    public int onMeasureR(int attr, int oldMeasure) {
        int newSize = 0;
        int mode    = MeasureSpec.getMode(oldMeasure);
        int oldSize = MeasureSpec.getSize(oldMeasure);

        switch (mode) {
            //父级已确定子级的确切大小。不管孩子有多大，他都会得到这些限制。
            case MeasureSpec.EXACTLY:
                newSize = oldSize;
                break;
            //子对象可以任意大到指定的大小
            case MeasureSpec.AT_MOST:
            //父对象未对子对象施加任何约束。它可以是它想要的任何尺寸。
            case MeasureSpec.UNSPECIFIED:
                float value;
                value = (mStrokeWidth / 2.5f + 40) * 2;
                newSize = (int) (getPaddingTop() + value + getPaddingBottom());
                break;
            default:
                break;
        }
        return newSize;
    }

    public int getColorValue() {
        return colorValue;
    }

    public void setColorValue(int colorValue) {
        this.colorValue = colorValue;
        //设置颜色
        paint.setColor(colorValue);
        invalidate();
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean isSelect) {
        this.isSelect = isSelect;
        if( isSelect ){
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
        }
        else{
            paint.setStyle(Paint.Style.STROKE);
        }
        invalidate();
    }

    public void setStrokeWidth(  int width){
        mStrokeWidth = width;
        paint.setStrokeWidth( mStrokeWidth );
        invalidate();
    }

}

