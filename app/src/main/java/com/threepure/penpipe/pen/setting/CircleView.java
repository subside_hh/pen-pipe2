package com.threepure.penpipe.pen.setting;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.threepure.penpipe.R;
import com.threepure.penpipe.util.Config;
import com.threepure.penpipe.util.DisplayUtil;

/**
 * @author ThreePure
 * @date 2021/11/15 19:34
 * @description: 选择笔画粗细的小圆点
 * @since 1.8
 */
public class CircleView extends View {

    /**绘制的中心圆圈*/
    private Paint paint;
    /**每一个CircleView对象后面都还一个像背景一样的圆*/
    private Paint backPaint;
    /**被选中后的外环【被选中后有一个圆环】*/
    private Paint borderPaint;

    /**圆圈的颜色*/
    private int paintColor;
    /**圆圈的半径*/
    private int circleRadius;
    /**布局文件中设置的圆圈的半径水平*/
    private int radiusLevel;

    /**是否显示被选中的外环*/
    private boolean showBorder = false;


    public CircleView(Context context) {
        this(context, null);
    }

    public CircleView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        //检索此上下文主题中的样式化属性信息。理解为与values下attrs.xml中定义的属性进行绑定
        TypedArray ta  = context.obtainStyledAttributes(attrs, R.styleable.CircleView);
        paintColor     = ta.getColor(R.styleable.CircleView_penColor, 0x8c000000);
        radiusLevel    = ta.getInteger(R.styleable.CircleView_sizeLevel, 1);
        showBorder     = ta.getBoolean(R.styleable.CircleView_showBorder, false);
        //圆的半径由xml中定义的sizeLevel认定为的下标确定，从Config.java中找到画笔大小集合PEN_SIZES。
        circleRadius   = DisplayUtil.dip2px(context, Config.PEN_SIZES[radiusLevel]);
        //回收Darray类型，供以后的调用方重新使用。调用此函数后，不得再次触摸键入的数组
        ta.recycle();
        //初始化画笔等 
        init();
    }

    /**
     * @description:  初始化画笔等
     * @Param: []
     * @Return: void
     */
    private void init() {
        //被选中后的外环粗细
        borderPaint = new Paint();
        borderPaint.setColor(paintColor);
        borderPaint.setStrokeWidth(5);
        borderPaint.setAntiAlias(true);
        borderPaint.setStrokeJoin(Paint.Join.ROUND);
        borderPaint.setStyle(Paint.Style.STROKE);

        //每一个CircleView对象后面都还一个像背景一样的圆
        backPaint = new Paint();
        backPaint.setColor(Color.TRANSPARENT);
        backPaint.setAntiAlias(true);
        backPaint.setStrokeJoin(Paint.Join.ROUND);
        backPaint.setStyle(Paint.Style.FILL);

        //绘制的中心圆圈
        paint = new Paint();
        paint.setColor(paintColor);
        paint.setStrokeWidth(20);
        paint.setAntiAlias(true);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //背景
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, getWidth() / 2, backPaint);

        //中心圆
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, circleRadius / 2.5f, paint);

        //绘制边框【被选中后有一个圆环】
        if (showBorder) {
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, circleRadius / 2.5f + 10, borderPaint);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width  = onMeasureR(0, widthMeasureSpec);
        int height = onMeasureR(1, heightMeasureSpec);
        setMeasuredDimension(width, height);
    }

    /**
     * @description: 用于计算控件的尺寸
     * @Param: [int, int]
     * @Return: int
     */
    public int onMeasureR(int attr, int oldMeasure) {
        int newSize = 0;
        int mode    = MeasureSpec.getMode(oldMeasure);
        int oldSize = MeasureSpec.getSize(oldMeasure);

        switch (mode) {
            //父级已确定子级的确切大小。不管孩子有多大，他都会得到这些限制。
            case MeasureSpec.EXACTLY:
                newSize = oldSize;
                break;
            //子对象可以任意大到指定的大小
            case MeasureSpec.AT_MOST:
            // 父对象未对子对象施加任何约束。它可以是它想要的任何尺寸。
            case MeasureSpec.UNSPECIFIED:
                float value;
                value = (circleRadius / 2.5f + 40) * 2;
                newSize = (int) (getPaddingTop() + value + getPaddingBottom());
                break;
            default:
                break;
        }
        return newSize;
    }


    public int getPaintColor() {
        return paintColor;
    }

    public void setPaintColor(int paintColor) {
        this.paintColor = paintColor;
        //设置中心圆的颜色
        paint.setColor(paintColor);
        invalidate();
    }

    public int getRadiusLevel() {
        return radiusLevel;
    }

    public void setRadiusLevel(int radiusLevel) {
        this.radiusLevel = radiusLevel;
        //设置圆的半径
        this.circleRadius = DisplayUtil.dip2px(getContext(), Config.PEN_SIZES[radiusLevel]);
        invalidate();
    }

    public boolean isShowBorder() {
        return showBorder;
    }

    public void setShowBorder(boolean showBorder) {
        //设置是否设置外环
        this.showBorder = showBorder;
        invalidate();
    }

}

