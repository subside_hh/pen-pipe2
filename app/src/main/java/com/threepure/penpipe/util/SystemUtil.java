package com.threepure.penpipe.util;

import static androidx.core.view.ViewCompat.getDisplay;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.RequiresApi;

/**
 * @author ThreePure
 * @date 2021/11/15 11:16
 * @description: 提供一些系统性服务（比如获取屏幕尺寸、判断是否平板设备等）
 * @since 1.8
 */
public class SystemUtil {

    private static final String TAG = "TP_SystemUtil=>";
    /**
     * 判断是否平板设备
     * @param context
     * @return true:平板, false:手机
     */
    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    /**
     * @description:  获取屏幕的宽度
     * @Param: [android.content.Context]
     * @Return: int
     */
    @RequiresApi(api = Build.VERSION_CODES.R)
    public static int getScreenWidth(Context context){
        //从系统服务中获取窗口管理器
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        int width = wm.getDefaultDisplay().getWidth();
        int height = wm.getDefaultDisplay().getHeight();
        //从默认显示显示器中获取显示参数保存到dm对象中
        wm.getDefaultDisplay().getMetrics(dm);
        //return dm.widthPixels;//返回屏幕的宽度数值

        return width;
    }

    /**
     * @description:  获取屏幕的高度
     * @Param: [android.content.Context]
     * @Return: int
     */
    public static int makeDropDownMeasureSpec(int measureSpec) {
        int mode;
        if (measureSpec == ViewGroup.LayoutParams.WRAP_CONTENT) {
            mode = View.MeasureSpec.UNSPECIFIED;
        } else {
            mode = View.MeasureSpec.EXACTLY;
        }
        return View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(measureSpec), mode);
    }
}
