package com.threepure.penpipe.util;

/**
 * @author ThreePure
 * @date 2021/11/17 15:06
 * @description:  触摸点信息
 * @since 1.8
 */
public class MotionElement {

    /**触摸点坐标*/
    public float x;
    public float y;

    /**触摸时的压力值  物理设备决定的，和设计的设备有关系*/
    public float pressure;

    /**绘制的工具是否是手指或者是触控笔*/
    public int toolType;

    /**
     * @description:  触控点的口燥方法
     * @Param: [float, float, float, int] 触控点X坐标、 触控点Y坐标、触摸时的压力值、 触摸类型（手指/触控笔）
     * @Return:
     */
    public MotionElement(float x, float y, float pressure, int toolType) {
        this.x = x;
        this.y = y;
        this.pressure = pressure;
        this.toolType = toolType;
    }
}
