package com.threepure.penpipe.util;

import android.net.Uri;

import java.net.URI;

/**
 * @author ThreePure
 * @date 2021/11/15 9:46
 * @description: 一些系统配置
 * @since 1.8
 */
public class Config {

    /**画笔颜色集合{黑色、红色、黄色、蓝色、绿色}*/
    public static final int[] PEN_COLORS = new int[]{0xFF000000, 0xFFFF0000, 0xFFFFFF00, 0xFF0000FF, 0xFF00FF00};
    /**画笔粗细集合*/
    public static final int[] PEN_SIZES  = new int[]{5, 10, 14, 17, 20};

    /**橡皮的大小，用作设定橡皮seekBar的长度*/
    public static int eraserSize = 25;

    /**是否开启定时器功能*/
    public static boolean openTimer = false;

    /**定时器执行清屏的时长 15s */
    public static int TIME_OF_CLEAR_SCREEN = 15000;

    /**画布的默认的背景颜色*/
    public static int Default_bg_Color = 0x88c8c8c8;

    /**用户自定义的画布的背景图片，默认为 null*/
    public static Uri Default_bg_image = null;

    /**当前用户设置是否为图片背景（不是图片背景则为颜色背景）*/
    public static boolean isImageBg = true;

    /**用户自定义的水印文字（10个字以内最佳） */
    public static String waterMaskText = null;

    /**画笔类型为手指*/
    public static final int PEN_TYPE_FINGER = 1;
    /**画笔类型为电容笔*/
    public static final int PEN_TYPE_ELECT  = 2;

    /**最终的图片是否只截取文字区域*/
    public static final boolean IS_CROP = true;

    /**保存图片的格式是PNG还是JPG*/
    public static final String SAVA_FORMAT = "PNG";








}
