package com.threepure.penpipe.util;

/**
 * @author ThreePure
 * @date 2021/11/17 10:17
 * @description: 每个点的控制，三个因素：笔的宽度，坐标, 透明数值
 * @since 1.8
 */
public class ControllerPoint {
    /**点的x、y坐标*/
    public float x;
    public float y;

    /**点的宽度*/
    public float width;
    /**透明度*/
    public int alpha = 255;

    public ControllerPoint() {
    }

    public ControllerPoint(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @description:  设置点的属性
     * @Param: [float, float, float]
     * @Return: void
     */
    public void set(float x, float y, float w) {
        this.x = x;
        this.y = y;
        this.width = w;
    }

    /**
     * @description:  设置点的属性
     * @Param: [com.threepure.penpipe.util.ControllerPoint]
     * @Return: void
     */
    public void set(ControllerPoint point) {
        this.x = point.x;
        this.y = point.y;
        this.width = point.width;
    }
}
