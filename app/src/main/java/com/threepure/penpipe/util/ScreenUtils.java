package com.threepure.penpipe.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.provider.Settings;

import android.view.Display;
import android.view.WindowManager;


/**
 * @author ThreePure
 * @date 2021/11/22 20:57
 * @description: Android 全面屏的和有导航栏的判断工具类
 * @since 1.8
 */
public class ScreenUtils {

    /**
     * @description:  获取屏幕宽度
     * @param:  sContext
     * @Return: int
     **/
    public static int getScreenWidth(Context sContext) {
        WindowManager wm = (WindowManager) sContext.getSystemService(Context.WINDOW_SERVICE);
        return wm.getDefaultDisplay().getWidth();
    }

    /**
     * @description:  获取屏幕的宽度
     * @Return: int
     **/
    public static int getScreenHeight(Context sContext) {
        WindowManager wm = (WindowManager) sContext.getSystemService(Context.WINDOW_SERVICE);
        return wm.getDefaultDisplay().getHeight();
    }

    /**
     * @description: 获取状态栏高度
     * @Return: int
     **/
    public static int getStatusBarHeight(Context sContext) {

        int result = 0;
        int resourceId = sContext.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = sContext.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * @description: 获取导航栏高度
     * @param: sContext
     * @Return: int
     **/
    public static int getNavigationBar(Context sContext){
        int result = 0;
        int resourceId = sContext.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = sContext.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }



    public static int dp2px(Context sContext, float dpValue) {
        final float scale = sContext.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public static int px2dp(Context sContext, float pxValue) {
        final float scale = sContext.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    public static int sp2px(Context sContext, float spValue) {
        final float fontScale = sContext.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }

    /**
     * 设置屏幕透明度
     * @param activity
     * @param f
     */
    public static void setScreentAlpha(Activity activity, float f) {
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = f;
        if (f == 1) {
            //不移除该Flag的话,可能出现黑屏的bug
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }else {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }
        activity.getWindow().setAttributes(lp);
    }


    private volatile static boolean mHasCheckAllScreen;
    private volatile static boolean mIsAllScreenDevice;
    /**
     * @description:  判断是否是全面屏
     * @param context
     * @Return: boolean
     **/
    public static boolean isAllScreenDevice(Context context) {

        if (mHasCheckAllScreen) {
            return mIsAllScreenDevice;
        }
        mHasCheckAllScreen = true;
        mIsAllScreenDevice = false;
        // 低于 API 21的，都不会是全面屏。。。
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return false;
        }
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            Display display = windowManager.getDefaultDisplay();
            Point point = new Point();
            display.getRealSize(point);
            float width, height;
            if (point.x < point.y) {
                width = point.x;
                height = point.y;
            } else {
                width = point.y;
                height = point.x;
            }
            if (height / width >= 1.97f) {
                mIsAllScreenDevice = true;
            }
        }
        return mIsAllScreenDevice;
    }

    /**
     * @description: 判断全面屏手机是否开启了底部导航栏
     * @param context
     * @Return: boolean
     **/
    public static boolean checkHasNavigationBar(Context context) {
        boolean hasNavigationBar = false;
        if ((Settings.Global.getInt(context.getContentResolver(), "force_fsg_nav_bar", 0) != 0)) {
            hasNavigationBar = true;
        }
        return hasNavigationBar;
    }

}
