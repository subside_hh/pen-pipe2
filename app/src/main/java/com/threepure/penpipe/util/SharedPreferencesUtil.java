package com.threepure.penpipe.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author ThreePure
 * @date 2021/11/26 15:13
 * @description: 判断是否是第一次开启应用的工具类
 * @since 1.8
 */
public class SharedPreferencesUtil {
    private static final String spFileName = "welcomePage";
    public static final String  FIRST_OPEN = "firstOpen";

    public static Boolean getBoolean(Context context, String strKey, Boolean strDefault) {
        SharedPreferences setPreferences = context.getSharedPreferences(spFileName, Context.MODE_PRIVATE);
        Boolean result = setPreferences.getBoolean(strKey, strDefault);
        return result;
    }

    public static void putBoolean(Context context, String strKey,Boolean strData) {
        SharedPreferences activityPreferences = context.getSharedPreferences(spFileName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = activityPreferences.edit();
        editor.putBoolean(strKey, strData);
        editor.commit();
    }
}
